// Aanderaa provides a communication interface for Aanderaa optodes.
package aanderaa

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
)

const (
	// Command terminator
	EOL = "\r\n"
	// Command prompt
	PROMPT = "\r\n"
	// Data record terminator in streaming mode
	EOR = "\r\n"
	// Response indicating a valid command
	CMD_OK = '#'
	// Response indicating an invalid command
	CMD_INVALID = '*'
	// Response to a wakeup from low-power mode
	WAKEUP = '!'
)

var BadCommand = errors.New("Invalid command")

type Device struct {
	port     io.ReadWriter
	respBuf  bytes.Buffer
	rmu, wmu *sync.Mutex
	lastCmd  string
}

func NewDevice(port io.ReadWriter) *Device {
	return &Device{
		port: port,
		rmu:  &sync.Mutex{},
		wmu:  &sync.Mutex{}}
}

func (d *Device) readUntil(marker []byte) (string, error) {
	d.rmu.Lock()
	defer d.rmu.Unlock()

	d.respBuf.Reset()
	tail := -len(marker)
	b := make([]byte, 1)
	for {
		n, err := d.port.Read(b)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}

		if err != nil {
			return d.respBuf.String(), err
		}

		d.respBuf.Write(b)
		tail++
		if tail < 0 {
			continue
		}

		if bytes.Equal(d.respBuf.Bytes()[tail:], marker) {
			return strings.TrimSuffix(d.respBuf.String(), string(marker)), nil
		}
	}

}

// Send sends a command to the Device
func (d *Device) Send(cmd string) error {
	d.wmu.Lock()
	defer d.wmu.Unlock()

	d.lastCmd = cmd
	_, err := fmt.Fprint(d.port, cmd, EOL)
	return err
}

// Recv returns a response from the Device.
func (d *Device) Recv() (string, error) {
	// Skip the command echo
	d.readUntil([]byte(EOL))

	resp := make([]string, 0)
loop:
	for {
		line, err := d.readUntil([]byte(PROMPT))
		if err != nil {
			return "", err
		}

		n := len(line)
		switch line[n-1] {
		case CMD_OK:
			resp = append(resp, "")
			break loop
		case CMD_INVALID:
			return "", fmt.Errorf("%q: %w", d.lastCmd, BadCommand)
		}

		resp = append(resp, line)
	}

	return resp[0], nil
}

// Exec sends a command to the Device and returns the response along with
// any error that occurs.
func (d *Device) Exec(cmd string) (string, error) {
	err := d.Send(cmd)
	if err != nil {
		return "", err
	}
	return d.Recv()
}

// Start puts the Device into streaming mode
func (d *Device) Start() error {
	return d.Send("reset")
}

// Stop takes the device out of streaming mode.
func (d *Device) Stop() error {
	_, err := d.Exec("stop")
	return err
}

// Stream returns a channel which will supply data records when the Device
// is in "streaming mode", e.g. after the "run" command has been
// sent.
func (d *Device) Stream(ctx context.Context) <-chan string {
	ch := make(chan string, 1)
	go func() {
		defer close(ch)
		for {
			text, err := d.readUntil([]byte(EOR))
			if err != nil {
				return
			}
			select {
			case <-ctx.Done():
				return
			case ch <- text:
			default:
			}
		}
	}()

	return ch
}
