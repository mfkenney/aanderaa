package aanderaa

import (
	"bytes"
	"errors"
	"reflect"
	"testing"
)

func TestParse(t *testing.T) {
	table := []struct {
		in, out string
		err     error
	}{
		{
			in:  "get enable text\r\nEnable Text ...\r\n#\r\n",
			out: "Enable Text ...",
		},
		{
			in:  "no response\r\n#\r\n",
			out: "",
		},
		{
			in:  "invalid command\r\n*\r\n",
			out: "",
			err: BadCommand,
		},
	}

	var b bytes.Buffer
	dev := NewDevice(&b)

	for _, e := range table {
		b.Write([]byte(e.in))
		resp, err := dev.Recv()
		if !reflect.DeepEqual(resp, e.out) {
			t.Errorf("Bad response; expected %q, got %q", e.out, resp)
		}
		if !errors.Is(err, e.err) {
			t.Errorf("Expected %v, got %v", e.err, err)
		}
	}
}
